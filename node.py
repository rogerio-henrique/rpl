import networkx as nx 

class Node:
    __allInstances = dict()

    def printNodes(self):
        for nodeId in Node.__allInstances:
            options = Node.__allInstances[nodeId].getOptions()
            print(options,end='\n')


    def __init__(self,_distanceFromRoot,_range, _id):
        self.__id = _id
        self.__range = _range
        self.__distanceFromRoot = _distanceFromRoot

        #Por enquanto, to fazendo o Rank igual a distancia do Root
        self.__rank = _distanceFromRoot
        self.__isRoot = False 

        #Se a distancia do no raiz eh zero, este eh o no raiz
        if(self.__distanceFromRoot == 0):
            self.__isRoot = True 

        #Adiciona no na lista de instancias criadas
        Node.__allInstances[self.__id] = self
        
        self.__parents = []
        self.__children = []

        self.__options = {
            'id': self.__id,
            'rank': self.__rank,
            'distanceFromRoot': self.__distanceFromRoot,
            'parents': self.__parents,
            'children': self.__children
        }


    def getOptions(self):
        return self.__options


    def readMessage(self,_msg, _options):
        if(_msg == 'DIO'):
            response = self.processDIOMsg(_options)

            if(response == 'DAO'):
                self.__parents.append(_options['id'])
                self.sendMessage(response,self.__options, _options['id'])

        elif(_msg == 'DAO'):
            self.__children.append(_options['id'])


    def sendMessage(self, _msg, _options, _id = 'all'):
        if(_id == 'all'):
            for nodeId in Node.__allInstances:
                node = Node.__allInstances[nodeId]
                options = node.getOptions()

                if(abs(options['distanceFromRoot'] - self.__options['distanceFromRoot'])\
                     <= self.__range):
                    node.readMessage(_msg, _options)

        else:
            node = Node.__allInstances[_id]
            options = node.getOptions()
            node.readMessage(_msg, _options)

    
    def processDIOMsg(self, _options):
        if(_options['distanceFromRoot'] < self.__distanceFromRoot):
            return 'DAO'