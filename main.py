import node

Node = node.Node

a = Node(0,15,1)
b = Node(15,5,2)
c = Node(15,5,3)
d = Node(20,5,4)
e = Node(20,5,5)

nodes = [a,b,c,d,e]


if __name__ == '__main__':
    for node in nodes:
        node.sendMessage('DIO',node.getOptions())
    c.printNodes()